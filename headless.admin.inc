<?php
/**
 * @file
 * Administrative page callbacks for the headless module.
 */

/**
 * Configuration options for headless setup.
 */
function headless_config_form($form, &$form_state) {
  $form['prerender_token'] = array(
    '#title' => 'Prerender.io Token',
    '#type' => 'textfield',
    '#default_value' => variable_get('prerender_token'),
    '#description' => t('The Prerender.io service will generate static snapshots of your page in order for search engines to more effectively index content.')
  );

  return system_settings_form($form);
}
